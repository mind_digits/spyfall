# coding=utf-8
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
#from threading import * #Timer, Thread
import telegram
import sqlite3
import logging
import datetime
from time import sleep
import random
import copy

import random
from sql import *
#from classes import *
from settings import *


def start(bot, update, args):
    uid = update.message.chat_id
    n = identification(uid)
    print('идентификация', n)
    bot.send_chat_action(chat_id=uid, action=telegram.ChatAction.TYPING)
    sleep(0.3)
    bot.send_message(chat_id=uid, text="❄️❄️❄️ Дорогие пользователи! К нам приближаются новогодние радости, "
                                       "поэтому мы представляем вам еще одного нашего бота - 🎅 Тайного Санту! "
                                       "@tayniysantabot Он поможет вашей компании друзей, коллег и семье распределить "
                                       "подарки друг другу и подарить классную атмосферу! Заходите! 🔥@tayniysantabot 🔥")
    bot.send_chat_action(chat_id=uid, action=telegram.ChatAction.TYPING)
    sleep(3)
    if len(args) == 0:
        if n == "new":
            bot.send_message(chat_id=uid, text="Привет, это классный бот для игры в Шпиона/Spyfall.\n"
                                          "Для игры тебе нужны живые друзья. Понимаю, это непросто, но ничего не поделаешь.\n"
                                          "А теперь срочно напиши мне свое имя.",
                             parse_mode=telegram.ParseMode.MARKDOWN)
        else:
            if get_user_state(uid) == 2:
                custom_keyboard = [['Новая игра'], ['Локации']]
            else:
                custom_keyboard = [['Локации']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
            if get_user_state(uid) == 3:
                bot.send_message(chat_id=uid, text="%s, ты в зале ожидания игры.\nМожешь выйти командой /cancel" % n,
                                 reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
            else:
                bot.send_message(chat_id=uid, text="%s, молодец, что вернулся!" % n,
                             reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    else:
        gid = args[0]
        if n == "new":
            set_user_state(uid, 11)
            add_to_queue(uid, gid)
            bot.send_message(chat_id=uid, text="Привет, это классный бот для игры в Шпиона/Spyfall.\n"
                                               "Тебя уже ждут в игре, но сначала нужно познакомиться.\n"
                                               "Теперь срочно напиши мне свое имя.",
                             parse_mode=telegram.ParseMode.MARKDOWN)
        else:
            if get_user_state(uid) == 2:
                join_game(bot, uid, gid)



def identification(uid):
    # ищем имя, если есть - возвращаем, в том числе new
    n = get_user_name(uid)
    if n is not None:
        return n
    # если нет - добавляем и возвращаем new
    else:
        add = addusertodb(uid)
        return "new"


def locations(bot, update):
    uid = update.message.from_user.id
    locs = get_all_locs()
    text = ''
    for i in locs:
        text = text + i + '\n'
    text = text[:-1]
    bot.send_message(chat_id=uid, text=text, parse_mode=telegram.ParseMode.MARKDOWN)


def rules(bot, update):
    uid = update.message.from_user.id
    bot.send_message(chat_id=uid, text="Правила смотри по ссылке.\nЭтот бот - полная аналогия настольной игры. "
                                       "Главное в игре - живое общение, хоть в одной комнате, хоть по удаленке.\n"
                                       "https://hobbyworld.ru/download/rules/SPY_rules_new-web.pdf")


def stats(bot, update):
    uid = update.message.from_user.id
    bot.send_message(chat_id=uid, text=get_stats())

def feedback(bot, update, args):
    uid = update.message.from_user.id
    if len(args):
        text = ''
        for i in args:
            text = text + i + ' '
        text = "Сообщение от юзера %s:\n" % uid + text
        bot.send_message(chat_id="182116723", text=text)
    else:
        bot.send_message(chat_id=uid, text="После команды feedback нужно ввести текст сообщения: /feedback привет")


def editname(bot, update, args):
    uid = update.message.from_user.id
    if len(args):
        name = ''
        for i in args:
            name = name + i + ' '
        name = name[:-1]
        if len(name) > 20:
            bot.send_message(chat_id=uid, text="Очень длинное имя, максимум 20 символов")
            return
        bad_symbols = "#*_><+=[]{}!?"
        if any(s in name for s in bad_symbols):
            bot.send_message(chat_id=uid, text="Нельзя использовать символы *#~ и т.д.")
            return
        set_user_name(uid, name)
        bot.send_message(chat_id=uid, text="Получилось, {}".format(name),
                         parse_mode=telegram.ParseMode.MARKDOWN)
    else:
        bot.send_message(chat_id=update.message.chat_id, text="После команды нужно ввести новое имя: /editname Иван")


def trista(bot, update, args):
    print("/300", update.message.chat_id, args)
    new_command = args[0]
    uid = update.message.chat_id
    bot.send_message(chat_id=uid, text='/' + new_command)
    nf = NewFunc(new_command)
    dyn_commands[new_command] = nf
    updater.dispatcher.add_handler(CommandHandler(new_command, dyn_commands[new_command].snd))


def spam(bot, update, args):
    uid = update.message.chat_id
    text = ''
    for i in args:
        text += i + " "
    text = text[0:-1]
    ulist = get_all_users()
    active = 0
    passive = 0
    for i in ulist:
        try:
            sleep(0.1)
            bot.send_message(chat_id=i, text=text)
            active += 1
        except:
            passive += 1
    text_res = f"Отправлено {active}\nБлок {passive}"
    bot.send_message(chat_id=uid, text=text_res)



def thanks(bot, update):
    print("/thanks", update.message.chat_id)
    text = "Тайный РобоСанта 🎅🏾 всегда рад устроить праздник для хороших мальчиков и девочек.\n" \
           "Если вы считаете, что Санта тоже был *хорошим мальчиком*, можете подкинуть ему пару угольков на нового " \
           "робо-оленя и гирлянду для серверной стойки.\n" \
           "Это можно сделать по одной из ссылок:" \
           "\nhttps://money.yandex.ru/to/41001753683166"\
           "\nhttps://www.tinkoff.ru/sl/2zLVSuJRSKI"
    bot.send_message(chat_id=update.message.from_user.id, text=text, parse_mode=telegram.ParseMode.MARKDOWN)
    bot.send_animation(chat_id=update.message.chat_id,
                       animation="https://media.giphy.com/media/9J8R3SaGMOhrK058Am/giphy.mp4")


def create_game(bot, uid):
    set_user_gameid(uid, uid)
    create_game_in_db(uid)
    set_user_state(uid, 3)
    mid = get_user_mes(uid)
    text = "Когда все зайдут, нажми кнопку Поехали, каждому придет локация " \
           "и начнется отсчет времени. Максимум 8 игроков.\n\nИгроки:"
    for i in get_game_users(uid):
        text = text + "\n" + i
    newgame_keyboard = [[InlineKeyboardButton("Поехали", callback_data='gogame:%s' % uid)],
                        [InlineKeyboardButton("Отмена", callback_data='cancelgame:%s' % uid)]]
    reply_markup = telegram.InlineKeyboardMarkup(newgame_keyboard)
    print('mid is', mid)
    if mid > 0: # если есть, что редактировать
        bot.edit_message_text(chat_id=uid, message_id=mid, text=text, reply_markup=reply_markup,
                              parse_mode=telegram.ParseMode.MARKDOWN)
        text = "https://t.me/game_spyfall_bot?start=%s\n" \
               "Играем в Шпиона! Нужно пройти по ссылке и нажать внизу Start/Начать" % uid
        custom_keyboard = [['Локации']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
        bot.send_message(chat_id=uid, text=text, reply_markup=reply_markup,
                         disable_web_page_preview=True)  # без маркдауна!
    else: # или отправляем новое сообщение
        mes = bot.send_message(chat_id=uid, text=text, reply_markup=reply_markup,
                              parse_mode=telegram.ParseMode.MARKDOWN)
        set_user_mes(uid, mes['message_id'])



def repeat(bot, update):
    uid = update.message.chat_id
    print("/repeat", uid)
    set_user_mes(uid, 0)
    new_command = 'again' + str(uid)
    bot.send_message(chat_id=uid, text='Команда для входа в новую игру. Отправил её всем участникам предыдущей.\n'
                                       '/' + new_command)
    nf = NewFunc(uid, join_game)
    dyn_commands[uid] = nf
    updater.dispatcher.add_handler(CommandHandler(new_command, dyn_commands[uid].join))

    last_game_id = get_last_game_rid(uid)
    userlist_ids = get_last_game_users(last_game_id)
    for i in userlist_ids:
        text = get_user_name(uid) + ' зовёт тебя поиграть еще разок!\nЖми на команду ниже\n/' + new_command
        bot.send_message(chat_id=i, text=text)
    create_game(bot, uid)


def join_game(bot, uid, gid=0):
    if get_user_state(uid) == 3:
        bot.send_message(text="Сперва выйди из другой игры\n", chat_id=uid)
        return 0
    elif get_user_state(uid) == 4:
        bot.send_message(text="Дождись окончания предыдущей игры, сливаться нельзя\n", chat_id=uid)
        return 0
    if gid == 0:  # новый чел
        gid = get_from_queue(uid)
        print('Получили игру из кармашка', gid)
    if check_game(gid, if_open=True):
        if len(get_game_users(gid)) < maxplayers:
            set_user_gameid(uid, gid)
            set_user_state(uid, 3)
            mes = bot.send_message(chat_id=uid, text="секундочку")
            set_user_mes(uid, mes['message_id'])
            # обновляем сообщения у всех
            userlist = ''
            for i in get_game_users(gid):
                userlist = userlist + "\n" + i
            for i in get_game_users(gid, i_need_id=True):
                if str(i) == str(gid):  # если админ
                    text = "Перешли следующее сообщение с ссылкой своим друзьям. Когда все зайдут, нажми кнопку Поехали, всем придет локация " \
                           "и начнется отсчет времени. Максимум 8 игроков.\n\nИгроки:" + userlist
                    custom_keyboard = [[InlineKeyboardButton("Поехали", callback_data='gogame:%s' % gid)],
                                       [InlineKeyboardButton("Отмена", callback_data='cancelgame:%s' % gid)]]
                else:
                    text = "Ты в игре!\nЖдем остальных.\n\nИгроки:" + userlist
                    custom_keyboard = [[InlineKeyboardButton("Выйти", callback_data='leavegame:%s' % gid)]]
                reply_markup = telegram.InlineKeyboardMarkup(custom_keyboard)
                bot.edit_message_text(text=text, chat_id=i, message_id=get_user_mes(i), reply_markup=reply_markup)
        else:
            bot.send_message(chat_id=uid, text="Упс, все 8 мест уже заняты.")
    else:
        bot.send_message(chat_id=uid, text="Я не нашел нужную игру.\n"
                                           "Либо она уже закончилась, либо создатель отменил её.\n"
                                           "Можешь создать ёё сам :)")



def finish_game(bot, gid):
    print("Завершается игра", gid)
    custom_keyboard = [['Новая игра'], ['Локации']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
    empty_markup = telegram.InlineKeyboardMarkup([[]])
    user_list_id = get_game_users(gid, i_need_id=True)
    for i in user_list_id:
        bot.edit_message_text(chat_id=i, message_id=get_user_mes(i), text="Игра завершена", reply_markup=empty_markup)
        if str(i) == str(gid):
            bot.send_message(chat_id=i, text="Надеюсь, вы нашли шпиона. Если нет, пора голосовать.\n"
                                             "А потом запустить еще одну игру."
                                             "\n\nЕсли хочешь создать новую игру и разослать тем же людям приглашения "
                                             "через бота, жми /repeat", reply_markup=reply_markup)
        else:
            bot.send_message(chat_id=i, text="Надеюсь, вы нашли шпиона. Если нет, пора голосовать.\n"
                                             "А потом запустить еще одну игру.", reply_markup=reply_markup)
        set_user_state(i, 2)
        set_user_mes(i, 0)
        set_user_gameid(i, 0)
    del_game(gid)

    # потом - спросить кто победил

def cancel_game(gid, bot):
    for i in get_game_users(gid, i_need_id=True):
        set_user_state(i, 2)
        empty_markup = telegram.InlineKeyboardMarkup([[]])
        mid = get_user_mes(i)
        set_user_mes(i, 0)
        sleep(0.1)
        custom_keyboard = [['Новая игра'], ['Локации']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
        bot.edit_message_text(chat_id=i, message_id=mid, text="Игра удалена", reply_markup=empty_markup,
                              parse_mode=telegram.ParseMode.MARKDOWN)
        bot.send_message(chat_id=i, text="Чем займемся?", reply_markup=reply_markup,
                         parse_mode=telegram.ParseMode.MARKDOWN)
    del_game(gid)  # всем, кто записался, сбрасываем активную игру на 0


def leave_game(gid, uid, bot, mid=0):
    set_user_state(uid, 2)
    set_user_mes(uid, 0)
    set_user_gameid(uid, 0)

    if mid:
        empty_markup = telegram.InlineKeyboardMarkup([[]])
        bot.edit_message_text(chat_id=uid, message_id=mid, text="Ты вышел", reply_markup=empty_markup)

    custom_keyboard2 = [['Новая игра'], ['Локации']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard2, resize_keyboard=True)
    bot.send_message(chat_id=uid, text="Чем займемся?", reply_markup=reply_markup)

    userlist = ''
    for i in get_game_users(gid):
        userlist = userlist + "\n" + i
    for i in get_game_users(gid, i_need_id=True):
        if str(i) == str(gid):  # если админ
            text = "Перешли следующее сообщение с ссылкой своим друзьям. Когда все зайдут, нажми кнопку Поехали, всем придет локация " \
                   "и начнется отсчет времени. Максимум 8 игроков.\n\nИгроки:" + userlist
            custom_keyboard = [[InlineKeyboardButton("Поехали", callback_data='gogame:%s' % gid)],
                               [InlineKeyboardButton("Отмена", callback_data='cancelgame:%s' % gid)]]
        else:
            text = "Ты в игре!\nЖдем остальных.\n\nИгроки:" + userlist
            custom_keyboard = [[InlineKeyboardButton("Выйти", callback_data='leavegame:%s' % gid)]]
        reply_markup = telegram.InlineKeyboardMarkup(custom_keyboard)
        bot.edit_message_text(text=text, chat_id=i, message_id=get_user_mes(i), reply_markup=reply_markup)


def leave_or_cancel(bot, update):
    uid = update.message.chat_id
    gid = get_user_gameid(uid)
    if gid == uid:
        cancel_game(gid, bot)
    else:
        leave_game(gid, uid, bot)
    bot.send_message(chat_id=uid, text="Готово, ты обнулился ;)", reply_markup=reply_markup)



# TODO нужны ли изменения при отмене посередине игры?

def warning30(bot, gid):
    user_list_id = get_game_users(gid, i_need_id=True)
    for i in user_list_id:
        bot.send_message(chat_id=i, text="Осталось 30 секунд")

def choose_gif(loc, role=0):
    if loc == 'Шпион':
        return spy_gif[random.randint(0, len(spy_gif) - 1)]
    else:
        result = loc_gif[loc]
        return result[random.randint(0, len(result) - 1)]



def react_inline(bot, update):
    query = update.callback_query
    mid = query.message.message_id
    print("QUERY:", query.data)
    uid = query.from_user['id']

    if 'cfn' in query.data:
        new_name = query.data.split(':')[1]
        set_user_name(uid, new_name)
        bot.send_message(chat_id="182116723", text="Новый юзер %s" % new_name, parse_mode=telegram.ParseMode.MARKDOWN)
        set_user_state(uid, 2)
        empty_markup = telegram.InlineKeyboardMarkup([[]])
        text_name_conf = "Окаюшки, записал\n\nПолезные команды:" \
                         "\n/rules - почитать правила" \
                         "\n/editname новое_имя - изменить имя" \
                         "\n/feedback текст - написать создателю"
        bot.edit_message_text(text=text_name_conf, chat_id=uid, message_id=mid,
                              reply_markup=empty_markup)
        if 'cfn1' in query.data:
            custom_keyboard = [['Новая игра'], ['Локации']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
            bot.send_message(chat_id=uid, text="Чем займемся? Можешь создать новую игру и пригласить друзей.",
                             reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        if 'cfn2' in query.data:
            join_game(bot, uid)

    if 'newgame' in query.data:
        set_user_mes(uid, mid)
        create_game(bot, uid)


    if 'gogame' in query.data:
        gid = query.data.split(':')[1]
        userlist_names = ''
        reply_markup = telegram.InlineKeyboardMarkup([[]])
        for i in get_game_users(gid):
            userlist_names = userlist_names + "\n" + i
        userlist_ids = get_game_users(gid, i_need_id=True)
        for i in userlist_ids:
            bot.edit_message_text(chat_id=i, message_id=get_user_mes(i), text="Игра запущена",
                                  reply_markup=reply_markup,
                                  parse_mode=telegram.ParseMode.MARKDOWN)
        if len(userlist_ids) < 2:
            bot.send_message(chat_id=uid, text="Минимум 3 человека. Позови еще друзей.")
            return 0
        id_in_log = get_last_game_rid() + 1
        for i in userlist_ids:  # стейты 4 всем
            set_user_state(i, 4)
            game_to_log(id_in_log, gid, i, datetime.date.today())
        print("закрываем игру для входа")
        open_close_game(gid, 0)
        print("выбираем локацию")
        locs = get_all_locs()
        loc_now = locs[random.randint(0, len(locs) - 1)]
        print("локация", loc_now)

        # выбираем шпиона
        spy_id = userlist_ids.pop(random.randint(0, len(userlist_ids)-1))
        print("Выбран шпион", spy_id)
        text = "Игра идёт!\n\n*Участники:*" + userlist_names + "\n\n*Ты Шпион!*" \
                                                               "\nДогадайся, в какой локации вы все находитесь и не спались.\n"
        if str(spy_id) == str(gid):
            admin_keyboard = [[InlineKeyboardButton("Закончить игру", callback_data='finishgame:%s' % gid)]]
            reply_markup = telegram.InlineKeyboardMarkup(admin_keyboard)
        else:
            reply_markup = telegram.InlineKeyboardMarkup([[]])
        mes = bot.send_message(chat_id=spy_id, text=text, reply_markup=reply_markup,
                               parse_mode=telegram.ParseMode.MARKDOWN)
        set_user_mes(spy_id, mes['message_id'])
        bot.send_animation(chat_id=spy_id, animation=choose_gif('Шпион'))

        print("выбираем роли")
        roles = get_roles(loc_now)
        print(roles)
        for i in userlist_ids:
            role_now = roles.pop(random.randint(0, len(roles) - 1))
            print("Назначена роль", i, role_now)
            text = "Игра идёт!\n\n*Участники:*" + userlist_names + "\n\nЛокация *" + loc_now + " *\nТы* " + role_now + "*"
            text += "\nЗадавай друзьям каверзные вопросы, найди Шпиона, который не знает локацию."
            if str(i) == str(gid):
                admin_keyboard = [[InlineKeyboardButton("Закончить игру", callback_data='finishgame:%s' % gid)]]
                reply_markup = telegram.InlineKeyboardMarkup(admin_keyboard)
            else:
                reply_markup = telegram.InlineKeyboardMarkup([[]])
            mes = bot.send_message(chat_id=i, text=text, reply_markup=reply_markup,
                                   parse_mode=telegram.ParseMode.MARKDOWN)
            set_user_mes(i, mes['message_id'])
            bot.send_animation(chat_id=i, animation=choose_gif(loc_now, role_now))

        # запускаем таймер 7-30
        warn_timer = PokaTimer(60 * 8 - 30, warning30, bot, gid, tid=gid)
        end_timer = PokaTimer(60*8, finish_game, bot, gid, tid=gid)
        games.timers.append(warn_timer)
        games.timers.append(end_timer)
        warn_timer.start()
        end_timer.start()

        dyn_commands.pop(gid, 0)



    if 'cancelgame' in query.data:
        gid = query.data.split(':')[1]
        cancel_game(gid, bot)


    if 'finishgame' in query.data:
        gid = query.data.split(':')[1]
        print("Нажали кнопку для завершения игры", gid)
        finish_game(bot, gid)
        for i in games.timers:
            if str(i.tid) == str(gid):
                i.cancel()


    if ('leavegame' in query.data):
        gid = query.data.split(':')[1]
        leave_game(gid, uid, mid, bot)





def echo(bot, update):
    print("USER:", update.message.text)
    uid = update.message.from_user.id
    n = identification(uid)
    if uid < 0:
        return
    if (update.message.text == "300"):
        bot.send_message(chat_id=update.message.chat_id, text="Ты сам знаешь, что делать")
    elif (get_user_state(uid) == 1) or (get_user_state(uid) == 11):
        new_name = update.message.text
        if len(new_name) > 25:
            bot.send_message(chat_id=update.message.chat_id, text="Очень длинное имя\nДавай покороче, а?")
            return
        bad_symbols = "#*_><+=[]{}!?"
        if any(s in new_name for s in bad_symbols):
            bot.send_message(chat_id=update.message.chat_id, text="Нельзя использовать символы *#~ и т.д.")
            return
        else:
            if get_user_state(uid) == 1:
                confirm_name_keyboard = [[InlineKeyboardButton("Да", callback_data='cfn1:%s' % new_name)]]
            elif get_user_state(uid) == 11:
                confirm_name_keyboard = [[InlineKeyboardButton("Да", callback_data='cfn2:%s' % new_name)]]
            reply_markup = telegram.InlineKeyboardMarkup(confirm_name_keyboard)
            bot.send_message(text="*%s* - это твое имя?\nМне запомнить тебя так?\nЕсли все ок, просто нажми Да.\n"
                                  "Если тебе не нравится это имя, просто введи новое" % new_name,
                                  chat_id=update.message.chat_id, message_id=update.message.message_id,
                                  reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    elif (update.message.text == "Локации"):
        uid = update.message.from_user.id
        locs = get_all_locs()
        text = ''
        for i in locs:
            text = text + i + '\n'
        text = text[:-1]
        bot.send_message(chat_id=uid, text=text, parse_mode=telegram.ParseMode.MARKDOWN)
    elif (update.message.text == "Новая игра"):
        if get_user_state(uid) == 2:
            newgame_keyboard = [[InlineKeyboardButton("Создать", callback_data='newgame')]]
            reply_markup = telegram.InlineKeyboardMarkup(newgame_keyboard)
            bot.send_message(text="Создать новую игру?\n"
                                  "Я сгенерирую ссылку, по которой твои друзья смогут присоединиться к игре.",
                             chat_id=uid, reply_markup=reply_markup)
        elif get_user_state(uid) == 3:
            bot.send_message(text="Сперва выйди из другой игры\n", chat_id=uid)
        elif get_user_state(uid) == 4:
            bot.send_message(text="Дождись окончания предыдущей игры, сливаться нельзя\n", chat_id=uid)

    else:
        bot.send_message(chat_id=update.message.chat_id, text="Как дела, *%s*?\nСкоро поиграем в Шпиона/Spyfall." % n,
                         parse_mode=telegram.ParseMode.MARKDOWN)



if __name__ == '__main__':
    dispatcher = updater.dispatcher
    dispatcher.add_handler(MessageHandler(Filters.text, echo))
    dispatcher.add_handler(CommandHandler("start", start, pass_args=True))
    dispatcher.add_handler(CommandHandler("locations", locations))
    dispatcher.add_handler(CommandHandler("rules", rules))
    dispatcher.add_handler(CommandHandler("editname", editname, pass_args=True))
    dispatcher.add_handler(CommandHandler("feedback", feedback, pass_args=True))
    dispatcher.add_handler(CommandHandler("300", trista, pass_args=True))
    dispatcher.add_handler(CommandHandler("repeat", repeat))
    dispatcher.add_handler(CommandHandler("stats", stats))
    dispatcher.add_handler(CommandHandler("spam", spam, pass_args=True))
    dispatcher.add_handler(CallbackQueryHandler(react_inline))
    dispatcher.add_handler(CommandHandler("cancel", leave_or_cancel))
    updater.start_polling()

    print('start_spyfall')
