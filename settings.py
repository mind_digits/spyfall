from telegram.ext import Updater

from classes import *

test = 0
#test = 1

if test:
    updater = Updater(token="328682751:AAHDnN7m4j_Ur_9Nb9DNGi-exuy2hQhpXZ4") #test
    #groupid = '-390912564' #тестовая
    botname = 'cami_buddy_bot'
else:
    updater = Updater(token="1259431708:AAEtTyKxkGJWHVHv9a50zv0Jc8MuYt3x7aA")  # game_spyfall_bot
    #groupid = '-1001343520717'  # продакшн
    botname = 'game_spyfall_bot'

admin_list = ['182116723']

games = Games()
users = Users()

maxplayers = 8


# loc_gif_demo = {12345}

loc_gif = {
    'Орбитальная станция': ['https://media.giphy.com/media/yKqEFne7oWs7e/giphy.gif',
                            'https://media.giphy.com/media/WQOjbfvfURJW9G1kFp/giphy.gif'],
    'Воинская часть': ['https://media.giphy.com/media/122CjzlQTX8jHW/giphy.gif'],
    'Университет': ['https://media.giphy.com/media/xm1Nzhnjfytby/giphy.gif'],
    'Полицейский участок': ['https://media.giphy.com/media/ksKIrjN3fdLTa/giphy.gif'],
    'Корпоративная вечеринка': ['https://media.giphy.com/media/EJMyMO22UxP68/giphy.gif'],
    'Подводная лодка': ['https://media.giphy.com/media/3ohs7UwMA2PCICFi4E/giphy.gif',
                        'https://media.giphy.com/media/117SivfVHt2Yc8/giphy.gif',
                        'https://media.giphy.com/media/ylG3QPf0MoCc0/giphy.gif'],
    'Пляж': ['https://media.giphy.com/media/Mh9DxE4smaKUE/giphy.gif'],
    'База террористов': ['https://media.giphy.com/media/l0HlD49piBZDdvjoI/giphy.gif',
                         'https://media.giphy.com/media/3o6ZtnrIF98uaXxO9y/giphy.gif'],
    'Отель': ['https://media.giphy.com/media/SsaWuR3owjU7a0G8z1/giphy.gif'],
    'Церковь': ['https://media.giphy.com/media/y0YlBkNIvSeME/giphy.gif'],
    'Казино': ['https://media.giphy.com/media/3o72FeJnjfZZ31WjBu/giphy.gif'],
    'Пиратский корабль': ['https://media.giphy.com/media/jVaDtT2jScTZOLld1P/giphy.gif',
                          'https://media.giphy.com/media/yoJC2vGOaUvMot6uic/giphy.gif'],
    'Больница': ['https://media.giphy.com/media/VbzpvRC1LWGRQAvues/giphy.gif'],
    'Войско крестоносцев': ['https://media.giphy.com/media/UFzjusdrC1EOc/giphy.gif'],
    'Театр': ['https://media.giphy.com/media/3o7TKOOVRs7gXDjRG8/giphy.gif'],
    'Самолет': ['https://media.giphy.com/media/R5RKVoy01UYdq/giphy.gif'],
    'Киностудия': ['https://media.giphy.com/media/KaPhVSRMEBl4s/giphy.gif',
                   'https://media.giphy.com/media/xT9KViaLtPX7HLTlaE/giphy.gif'],
    'Посольство': ['https://media.giphy.com/media/xT5LMFn2wiSY4I6hYA/giphy.gif'],
    'Полярная станция': ['https://media.giphy.com/media/3orieR2qWBWrGCMqKk/giphy.gif'],
    'Супермаркет': ['https://media.giphy.com/media/DZgUVLxzMoAwg/giphy.gif'],
    'Цирк-шапито': ['https://media.giphy.com/media/l0HlEpZsqp6lozBaU/giphy.gif'],
    'Банк': ['https://media.giphy.com/media/3oKHWlnesqVHLbZFOo/giphy.gif'],
    'Океанский лайнер': ['https://media.giphy.com/media/Hw8vYF4DNRCKY/giphy.gif'],
    'Станция техобслуживания': ['https://media.giphy.com/media/l41Ynm9kWA7pEsNgs/giphy.gif'],
    'Спа-салон': ['https://media.giphy.com/media/xT5LMtNAWcVh4zCdaw/giphy.gif'],
    'Пассажирский поезд': ['https://media.giphy.com/media/FYxJvUs2sb0o8/giphy.gif'],
    'Овощебаза': ['https://media.giphy.com/media/l3JDD43N56eVfsdi0/giphy.gif'],
    'Ресторан': ['https://media.giphy.com/media/3oz8xsbBifGU9uGspi/giphy.gif'],
    'Партизанский отряд': ['https://media.giphy.com/media/HogIqlxKxv3IQ/giphy.gif'],
    'Школа': ['https://media.giphy.com/media/KiXl0vfc9XIIM/giphy.gif'],
    'Стрипбар': ['https://media.giphy.com/media/cyIqp21yny1HO/giphy.gif'],
    'Психобольница': ['https://media.giphy.com/media/LqgBjcmCh21m8CGVrY/giphy.gif'],
    'Белый дом': ['https://media.giphy.com/media/hwC4NRJL7VUn6/giphy.gif'],
    'ЗАГС': ['https://media.giphy.com/media/h2BQTGMkZ8vm9jMnd9/giphy.gif'],
    'Следственный изолятор': ['https://media.giphy.com/media/QAtLRc6EvY7YgchGKn/giphy.gif'],
    'Суд': ['https://media.giphy.com/media/IYcXTDme1ZPMI/giphy.gif'],
    }


loc_base = {
    'Орбитальная станция': {
        'loc_gifs': [
            'https://media.giphy.com/media/yKqEFne7oWs7e/giphy.gif',
            'https://media.giphy.com/media/WQOjbfvfURJW9G1kFp/giphy.gif'],
        'roles': {
            'Инженер-механик': 0,
            'Пилот': 0,
            'Инженер-исследователь': 0,
            'Космический турист': 0,
            'Врач': 0,
            'Инопланетянин': 'https://media.giphy.com/media/brjJHcjSkifte/giphy.gif',
            'Командир': 0}},
    'Воинская часть': {
        'loc_gifs': [
            'https://media.giphy.com/media/122CjzlQTX8jHW/giphy.gif'],
        'roles': {
            'Полковник': 0,
            'Дезертир': 0,
            'Врач': 0,
            'Младший офицер': 0,
            'Караульный': 0,
            'Рядовой': 0,
            'Продавец в магазине': 0}},
    'Университет': {
        'loc_gifs': [
            'https://media.giphy.com/media/xm1Nzhnjfytby/giphy.gif'],
        'roles': {
            'Студент': 0,
            'Ректор': 0,
            'Аспирант': 0,
            'Вахтер': 0,
            'Профессор': 0,
            'Психолог': 0,
            'Декан': 0}},
    'Полицейский участок': {
        'loc_gifs': [
            'https://media.giphy.com/media/ksKIrjN3fdLTa/giphy.gif'],
        'roles': {
             'Преступник': 0,
             'Лейтенант': 0,
             'Адвокат': 0,
             'Детектив': 0,
             'Архивариус': 0,
             'Журналист': 0,
             'Патрульный': 0}},
    'Корпоративная вечеринка': {
        'loc_gifs': [
            'https://media.giphy.com/media/EJMyMO22UxP68/giphy.gif'],
        'roles': {
            'Незваный гость': 0,
            'Директор по персоналу': 0,
            'Ведущий': 0,
            'Курьер': 0,
            'Менеджер': 0,
            'Секретарь': 0,
            'Бухгалтер': 0}},
    'Подводная лодка': {
        'loc_gifs': [
            'https://media.giphy.com/media/3ohs7UwMA2PCICFi4E/giphy.gif',
            'https://media.giphy.com/media/117SivfVHt2Yc8/giphy.gif',
            'https://media.giphy.com/media/ylG3QPf0MoCc0/giphy.gif'],
        'roles': {
            'Матрос': 0,
            'Штурман': 0,
            'Электромеханик': 0,
            'Кок': 0,
            'Рад   т': 0,
            'Ракетчик': 0,
            'Гидроаккустик': 0}},
    'Пляж': {
        'loc_gifs': [
            'https://media.giphy.com/media/Mh9DxE4smaKUE/giphy.gif'],
        'roles': {
             'Вор': 0,
             'Спасатель': 0,
             'Отдыхающий': 0,
             'Фотогоаф с обезьянкой': 0,
             'Парапланерист': 0,
             'Разносчик еды': 0,
             'Аниматор': 0}},
    'База террористов': {
        'loc_gifs': [
            'https://media.giphy.com/media/l0HlD49piBZDdvjoI/giphy.gif',
            'https://media.giphy.com/media/3o6ZtnrIF98uaXxO9y/giphy.gif'],
        'roles': {
            'Медик': 0,
            'Новобранец': 0,
            'Мальчик на побегушках': 0,
            'Пастух': 0,
            'Смертник': 0,
            'Боевик': 0,
            'Глава ячейки': 0}},
    'Отель': {
        'loc_gifs': [
            'https://media.giphy.com/media/SsaWuR3owjU7a0G8z1/giphy.gif'],
        'roles': {
            'Охранник': 0,
            'Постоялец': 0,
            'Бармен': 0,
            'Портье': 0,
            'Горничная': 0,
            'Управляющий': 0,
            'Швейцар': 0}},
    'Церковь': {
        'loc_gifs': [
            'https://media.giphy.com/media/y0YlBkNIvSeME/giphy.gif'],
        'roles': {
            'Меценат': 0,
            'Хорист': 0,
            'Прихожанин': 0,
            'Нищий': 0,
            'Священник': 0,
            'Турист': 0,
            'Грешник': 0}},
    'Казино': {
        'loc_gifs': [
            'https://media.giphy.com/media/3o72FeJnjfZZ31WjBu/giphy.gif'],
        'roles': {
            'Начальник охраны': 0,
            'Вышибала': 0,
            'Крупье': 0,
            'Шулер': 0,
            'Администратор': 0,
            'Азартный посетитель': 0,
            'Бармен': 0}},
    'Пиратский корабль': {
        'loc_gifs': [
            'https://media.giphy.com/media/jVaDtT2jScTZOLld1P/giphy.gif',
            'https://media.giphy.com/media/yoJC2vGOaUvMot6uic/giphy.gif'],
        'roles': {
            'Раб': 0,
            'Канонир': 0,
            'Кок': 0,
            'Связанный пленник': 0,
            'Матрос': 0,
            'Юнга': 0,
            'Бравый капитан': 0}},
    'Больница': {
        'loc_gifs': [
            'https://media.giphy.com/media/VbzpvRC1LWGRQAvues/giphy.gif'],
        'roles': {
            'Главврач': 0,
            'Пациент': 0,
            'Терапевт': 0,
            'Медсестра': 0,
            'Хирург': 0,
            'Интерн': 0,
            'Патологоанатом': 0}},
    'Войско крестоносцев': {
        'loc_gifs': [
            'https://media.giphy.com/media/UFzjusdrC1EOc/giphy.gif'],
        'roles': {
            'Пленный сарацин': 0,
            'Монах': 0,
            'Слуга': 0,
            'Епископ': 0,
            'Оруженосец': 0,
            'Лучник': 0,
            'Рыцарь': 0}},
    'Театр': {
        'loc_gifs': [
            'https://media.giphy.com/media/3o7TKOOVRs7gXDjRG8/giphy.gif'],
        'roles': {
            'Гардеробщик': 0,
            'Зритель': 0,
            'Билетер': 0,
            'Суфлер': 0,
            'Рабочий сцены': 0,
            'Актер': 0,
            'Режисер': 0}},
    'Самолет': {
        'loc_gifs': [
            'https://media.giphy.com/media/R5RKVoy01UYdq/giphy.gif'],
        'roles': {
            'Командир экипажа': 0,
            'Стюард': 0,
            'Пассажир эконом-класса': 0,
            'Безбилетник': 0,
            'Пассажир бизнес-класса': 0,
            'Бортинженер': 0,
            'Второй пилот': 0}},
    'Киностудия': {
        'loc_gifs': [
            'https://media.giphy.com/media/KaPhVSRMEBl4s/giphy.gif',
            'https://media.giphy.com/media/xT9KViaLtPX7HLTlaE/giphy.gif'],
        'roles': {
            'Статист': 0,
            'Актер': 0,
            'Костюмер': 0,
            'Каскадер': 0,
            'Звукооператор': 0,
            'Оператор': 0,
            'Режисер': 0}},
    'Посольство': {
        'loc_gifs': [
            'https://media.giphy.com/media/xT5LMFn2wiSY4I6hYA/giphy.gif'],
        'roles': {
            'Секретарь': 0,
            'Турист': 0,
            'Дипломат': 0,
            'Охранник': 0,
            'Посол': 0,
            'Чиновник': 0,
            'Беженец': 0}},
    'Полярная станция': {
        'loc_gifs': [
            'https://media.giphy.com/media/3orieR2qWBWrGCMqKk/giphy.gif'],
        'roles': {
            'Медик': 0,
            'Метеоролог': 0,
            'Биолог': 0,
            'Геофизик': 0,
            'Начальник экспедиции': 0,
            'Радист': 0,
            'Гидролог': 0}},
    'Супермаркет': {
        'loc_gifs': [
            'https://media.giphy.com/media/DZgUVLxzMoAwg/giphy.gif'],
        'roles': {
            'Уборщик': 0,
            'Охранник': 0,
            'Мерчендайзер': 0,
            'Покупатель': 0,
            'Кассир': 0,
            'Промоутер': 0,
            'Мясник': 0}},
    'Цирк-шапито': {
        'loc_gifs': [
            'https://media.giphy.com/media/l0HlEpZsqp6lozBaU/giphy.gif'],
        'roles': {
            'Фокусник': 0,
            'Жонглер': 0,
            'Посетитель': 0,
            'Метатель ножей': 0,
            'Акробат': 0,
            'Дрессировщик': 0,
            'Клоун': 0}},
    'Банк': {
        'loc_gifs': [
            'https://media.giphy.com/media/3oKHWlnesqVHLbZFOo/giphy.gif'],
        'roles': {
            'Клиент': 0,
            'Ипотечник': 0,
            'Консультант': 0,
            'Грабитель': 0,
            'Инкассатор': 0,
            'Кассир': 0,
            'Управляющий': 0}},
    'Океанский лайнер': {
        'loc_gifs': [
            'https://media.giphy.com/media/Hw8vYF4DNRCKY/giphy.gif'],
        'roles': {
            'Капитан': 0,
            'Бармен': 0,
            'Музыкант': 0,
            'Кок': 0,
            'Богатый пассажир': 0,
            'Стюард': 0,
            'Радист': 0}},
    'Станция техобслуживания': {
        'loc_gifs': [
            'https://media.giphy.com/media/l41Ynm9kWA7pEsNgs/giphy.gif'],
        'roles': {
            'Мойщик': 0,
            'Шиномонтажник': 0,
            'Мотоциклист': 0,
            'Директор': 0,
            'Автомобилист': 0,
            'Мастер-приемщик': 0,
            'Электрик': 0}},
    'Спа-салон': {
        'loc_gifs': [
            'https://media.giphy.com/media/xT5LMtNAWcVh4zCdaw/giphy.gif'],
        'roles': {
            'Косметолог': 0,
            'Клиент': 0,
            'Стилист': 0,
            'Дерматолог': 0,
            'Визажист': 0,
            'Маникюрщик': 0,
            'Массажист': 0}},
    'Пассажирский поезд': {
        'loc_gifs': [
            'https://media.giphy.com/media/FYxJvUs2sb0o8/giphy.gif'],
        'roles': {
            'Повар вагона-ресторана': 0,
            'Пассажир': 0,
            'Проводник': 0,
            'Кочегар': 0,
            'Пограничник': 0,
            'Разносчик товаров': 0,
            'Машинист': 0}},
    'Овощебаза': {
        'loc_gifs': [
            'https://media.giphy.com/media/l3JDD43N56eVfsdi0/giphy.gif'],
        'roles': {
            'Лаборант': 0,
            'Охранник': 0,
            'Бригадир': 0,
            'Санинспектор': 0,
            'Бухгалтер': 0,
            'Водитель': 0,
            'Грузчик': 0}},
    'Ресторан': {
        'loc_gifs': [
            'https://media.giphy.com/media/3oz8xsbBifGU9uGspi/giphy.gif'],
        'roles': {
            'Музыкант': 0,
            'Посетитель': 0,
            'Шеф-повар': 0,
            'Метрдотель': 0,
            'Официант': 0,
            'Вышибала': 0,
            'Критик': 0}},
    'Партизанский отряд': {
        'loc_gifs': [
            'https://media.giphy.com/media/HogIqlxKxv3IQ/giphy.gif'],
        'roles': {
            'Повар': 0,
            'Пленный полицай': 0,
            'Медик': 0,
            'Разведчик': 0,
            'Радист': 0,
            'Партизан': 0,
            'Солдат': 0}},
    'Школа': {
        'loc_gifs': [
            'https://media.giphy.com/media/KiXl0vfc9XIIM/giphy.gif'],
        'roles': {
            'Уборщик': 0,
            'Директор': 0,
            'Школьник': 0,
            'Физрук': 0,
            'Классный руководитель': 0,
            'Охранник': 0,
            'Завуч': 0}},
    'Стрипбар': {
        'loc_gifs': [
            'https://media.giphy.com/media/cyIqp21yny1HO/giphy.gif'],
        'roles': {
            'Стриптизерша': 0,
            'Алкоголик': 0,
            'Диджей': 0,
            'Уборщик': 0,
            'Начальник охраны': 0,
            'Извращенец': 0,
            'Богатый клиент': 0}},
    'Психбольница': {
        'loc_gifs': [
            0],
        'roles': {
            'Шизик': 0,
            'Охранник': 0,
            'Санитар': 0,
            'Медсестра': 0,
            'Сталин': 0,
            'Наполеон': 0,
            'Гитлер': 0}},
    'Белый дом': {
        'loc_gifs': [
            0],
        'roles': {
            'Президент': 0,
            'Министр обороны': 0,
            'Телохранитель': 0,
            'Папарацци': 0,
            'Секретарь': 0,
            'Рептилоид': 0,
            'Террорист': 0}},
    'ЗАГС': {
        'loc_gifs': [
            0],
        'roles': {
            'Жених': 0,
            'Невеста': 0,
            'Регистратор': 0,
            'Свидетель': 0,
            'Свидетельница': 0,
            'Теща': 0,
            'Фотограф': 0}},
    'Следственный изолятор': {
        'loc_gifs': [
            0],
        'roles': {
            'Маньяк': 0,
            'Барыга': 0,
            'Педофил': 0,
            'Следователь': 0,
            'Криминалист': 0,
            'Невиновный': 0,
            'Петух': 0}},
    'Суд': {
        'loc_gifs': [
            0],
        'roles': {
            'Подозреваемый': 0,
            'Невиновный': 0,
            'Пацан к успеху': 0,
            'Адвокат': 0,
            'Судья': 0,
            'Прокурор': 0,
            'Присяжный': 0}},
    }

spy_gif = [
    'https://media.giphy.com/media/7OX5TxSALQdkWiFbCR/giphy.gif',
    'https://media.giphy.com/media/ZNFmtw8hXOJu8/giphy.gif',
    'https://media.giphy.com/media/1gRtl9mdLQvvvOfR9u/giphy.gif'
    ]