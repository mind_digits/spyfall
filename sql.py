# coding=utf-8
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from threading import * #Timer, Thread
import telegram
import sqlite3
import logging
import datetime
from time import sleep
import random
import copy

from sql import *
from classes import *
from settings import *

def addusertodb(id):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT * FROM users WHERE userid = ?"
    cursor.execute(sql, (id,))
    a = cursor.fetchall()
    if len(a) == 0:
        print('Новенький, добавляю', id)
        cursor.execute("""INSERT INTO users (userid, name, state, gameid, mesid) VALUES (?,?,?,?,?)""", (id, "new", 1, 0, 0))
        conn.commit()
        return 1
    else:
        print('Есть такой юзер в этой группе', a[0][0])
        return 0


def create_game_in_db(gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT gameid FROM games WHERE gameid = ?"
    cursor.execute(sql, (gid,))
    a = cursor.fetchall()
    if len(a):
        cursor.execute("""UPDATE games SET open = (?) WHERE gameid = (?)""", (1, gid))
    else:
        cursor.execute("""INSERT INTO games (gameid, open) VALUES (?,?)""", (gid, 1))
    conn.commit()
    print('Игра создана')


def add_to_queue(uid, gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT userid FROM queue WHERE userid = ?"
    cursor.execute(sql, (uid,))
    a = cursor.fetchall()
    if len(a):
        cursor.execute("""UPDATE queue SET gameid = (?) WHERE userid = (?)""", (gid, uid))
    else:
        cursor.execute("""INSERT INTO queue (userid, gameid) VALUES (?,?)""", (uid, gid))
    conn.commit()
    print('Юзер в очереди')


def get_from_queue(uid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT gameid FROM queue WHERE userid = ?"
    cursor.execute(sql, (uid,))
    a = cursor.fetchall()
    if len(a):
        sql = "DELETE FROM queue WHERE userid = ?"
        cursor.execute(sql, (uid,))
        conn.commit()
        return a[0][0]
    else:
        print("Нет нашел в очереди юзера", uid)
        return 0


def new_loc(loc, role):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""INSERT INTO locations (location, role) VALUES (?,?)""", (loc, role))
    conn.commit()


def get_loc_gif(loc):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT gif FROM locations WHERE location = ?"
    cursor.execute(sql, (loc,))
    a = cursor.fetchall()
    if len(a):
        return a[0][0]
    else:
        return 0


def set_loc_gif(loc, gif):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""UPDATE locations SET gif  = (?) WHERE location = (?)""", (gif, loc))
    conn.commit()


def set_user_name(id, name):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    print('Новое имя', name, 'для', id)
    cursor.execute("""UPDATE users SET name  = (?) WHERE userid = (?)""", (name, id))
    conn.commit()


def set_user_mes(uid, mesid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""UPDATE users SET mesid  = (?) WHERE userid = (?)""", (mesid, uid))
    conn.commit()
    # print('Присвоил mesid', mesid, 'юзеру', uid)


def get_user_mes(uid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT mesid FROM users WHERE userid = ?"
    cursor.execute(sql, (uid,))
    a = cursor.fetchall()
    if len(a):
        return a[0][0]
    else:
        return 0


def get_user_state(id):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT state FROM users WHERE userid = ?"
    cursor.execute(sql, (id,))
    a = cursor.fetchall()
    if len(a):
        return a[0][0]
    else:
        return 0


def set_user_state(id, st):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""UPDATE users SET state = (?) WHERE userid = (?)""", (st, id))
    conn.commit()


def set_user_gameid(uid, gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""UPDATE users SET gameid = (?) WHERE userid = (?)""", (gid, uid))
    conn.commit()
    print('Присвоил юзеру gameid', uid, gid)


def get_user_gameid(uid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT gameid FROM users WHERE userid = ?"
    cursor.execute(sql, (uid,))
    a = cursor.fetchall()
    if len(a):
        return a[0][0]
    else:
        print('Не нашел такого юзера', uid)
        return None


def get_user_name(uid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT name FROM users WHERE userid = ?"
    cursor.execute(sql, (uid,))
    a = cursor.fetchall()
    if len(a):
        return a[0][0]
    else:
        print('Не нашел такого юзера', uid)
        return None


def get_game_users(gid, i_need_id=False):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if i_need_id:
        sql = "SELECT userid FROM users WHERE gameid = ?"
    else:
        sql = "SELECT name FROM users WHERE gameid = ?"
    cursor.execute(sql, (gid,))
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        print("Показываем участников игры", gid)
        for i in a:
            b.append(i[0])
    else:
        pass
        print("Не нашел ни одного юзера для игры", gid)
    print(b)
    return b

def get_all_users():
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT userid FROM users"
    cursor.execute(sql)
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        for i in a:
            b.append(i[0])
    return b

def open_close_game(gid, op):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if op == 0:
        cursor.execute("""UPDATE games SET open = (?) WHERE gameid = (?)""", (0, gid))
    if op == 1:
        cursor.execute("""UPDATE games SET open = (?) WHERE gameid = (?)""", (1, gid))
    conn.commit()



def del_game(gid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""DELETE FROM games WHERE gameid = (?)""", (gid, ))
    conn.commit()
    cursor.execute("""UPDATE users SET gameid = (?) WHERE gameid = (?)""", (0, gid))
    conn.commit()
    print("Игра удалена %s" % gid)


def check_game(gid, if_open=False):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if if_open:
        sql = "SELECT gameid FROM games WHERE gameid = (?) AND open = 1"
        cursor.execute(sql, (gid,))
    else:
        sql = "SELECT gameid FROM games WHERE gameid = ?"
        cursor.execute(sql, (gid, ))
    a = cursor.fetchall()
    if len(a) > 0:
        return 1
    else:
        print("Нет такой игры %s" % gid)
        return 0


def get_all_locs():
    # conn = sqlite3.connect("mydb.ext")
    # cursor = conn.cursor()
    # sql = "SELECT DISTINCT location FROM locations"
    # cursor.execute(sql)
    # a = cursor.fetchall()
    # b = []
    # if len(a) > 0:
    #     for i in a:
    #         b.append(i[0])
    # return b
    newlist = list()
    for i in loc_base.keys():
        newlist.append(i)
    return newlist


def get_roles(loc):
    # conn = sqlite3.connect("mydb.ext")
    # cursor = conn.cursor()
    # sql = "SELECT role FROM locations WHERE location = (?)"
    # cursor.execute(sql, (loc,))
    # a = cursor.fetchall()
    # b = []
    # if len(a) > 0:
    #     for i in a:
    #         b.append(i[0])
    # print(b)
    newlist = list()
    for i in loc_base[loc]['roles'].keys():
        newlist.append(i)
    return newlist




def game_to_log(rnd, gid, uid, date):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    cursor.execute("""INSERT INTO games_log (randid, gameid, userid, gamedate) VALUES (?,?,?,?)""", (rnd, gid, uid, date))
    conn.commit()


def get_last_game_rid(gid=0):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    if gid == 0:
        sql = "SELECT MAX(randid) FROM games_log"
        cursor.execute(sql)
    else:
        sql = "SELECT MAX(randid) FROM games_log WHERE gameid = (?)"
        cursor.execute(sql, (gid,))
    a = cursor.fetchall()
    if len(a):
        return a[0][0]


def get_last_game_users(rid):
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "SELECT userid FROM games_log WHERE randid = (?) AND userid <> gameid"
    cursor.execute(sql, (rid,))
    a = cursor.fetchall()
    b = []
    if len(a) > 0:
        print("Показываем участников игры", rid)
        for i in a:
            b.append(i[0])
    else:
        pass
        print("Не нашел ни одного юзера для игры", rid)
    print(b)
    return b

def get_stats():
    conn = sqlite3.connect("mydb.ext")
    cursor = conn.cursor()
    sql = "select count(userid) from users"
    cursor.execute(sql)
    a = cursor.fetchall()

    sql = "select count(distinct randid) from games_log"
    cursor.execute(sql)
    b = cursor.fetchall()
    res = 'Юзеров: {}\nИгр: {}'.format(a[0][0], b[0][0])
    return res
