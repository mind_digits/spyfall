
#from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer # python2
from http.server import BaseHTTPRequestHandler, HTTPServer  # python3
import urllib.parse
import sqlite3
from classes import *
from threading import * #Timer, Thread
#from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from socketserver import ThreadingMixIn




class HandleRequests(BaseHTTPRequestHandler):
    timeout = 2
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()

        #print("Пришел GET запрос,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        pampam = urllib.parse.parse_qs(self.path[2:])
        print(pampam)
        if pampam.get("param")[0] == "led1":
            jarvis.led1 = 1

        conn = sqlite3.connect("mydb.ext")
        cursor = conn.cursor()
        sql = "SELECT ledstate FROM led WHERE ledid = ?"
        cursor.execute(sql, (1,))
        a = cursor.fetchall()
        if len(a) > 0:
            if a[0][0] == "0":
                self.wfile.write("screen_stop".format(self.path).encode('utf-8'))
            elif a[0][0] == "1":
                self.wfile.write("screen_down".format(self.path).encode('utf-8'))
            elif a[0][0] == "2":
                self.wfile.write("screen_up".format(self.path).encode('utf-8'))
        conn.commit()



    def do_POST(self):
        '''Reads post request body'''
        self._set_headers()
        content_len = int(self.headers.getheader('content-length', 0))
        post_body = self.rfile.read(content_len)
        self.wfile.write("received post request:<br>{}".format(post_body))



    def do_PUT(self):
        self.do_POST()


class ServThread(Thread):
    def __init__(self, name):
        Thread.__init__(self)
        self.name = name

    def run(self):
        host = ''
        port = 80
        print("Запускаем сервер")
        HTTPServer((host, port), HandleRequests).serve_forever()
        print("Запустили сервер")

jarvis = JarvisClass()

my_thread = ServThread("s1")
my_thread.start()